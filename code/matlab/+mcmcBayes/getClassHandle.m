function handle=getClassHandle(strType)
    if (exist(strType)==8)%is it a valid class on the path
        evalStr=sprintf('handle=@%s;',strType);%construct the expression to get the handle
        eval(evalStr);%get the handle
    else
        error('%s is not a valid class on the path.',strType);
    end
end