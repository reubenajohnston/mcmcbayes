classdef model < handle
    %MODEL 
    %   Base class for running functions that are specific to a model.  Example models are the linear and polynomialDegree2 regression
    % models.  Children of this class should be located in an external repository that is on the path and is dedicated to the custom model 
    % and its dependencies.

    properties (Constant, Access = private) %define as private so that subclasses can have their own instances with same name
        version='1.0';  % API version for this class
    end

    properties
        group   % Model group collection of similar types (e.g., regression family for linear and polynomial degree-2 models)
        type    % Model type (e.g., linear regression)
        subtype % Model subtype (e.g., instance of linear regression with flat() prior distributions for beta0, beta1, and r variables)
    end
    
    methods (Abstract)
        [loglikelihood]=computeLogLikelihood(obj, data, samples);           %Computation of the log-likelihood of data given a particular sample
        [varElementId]=getPhiVarElementId(obj);                             %Retrieves the variable index for phi given the model type+subtype
        [varElementIds]=getVarElementIds(obj);                              %Retrieves a list of variable element names+indices given the model type+subtype
        [datElementIds]=getDatElementIds(obj);                              %Retrieves a list of data element names+indices given the model type+subtype
        [retPredictions]=generatePredictions(obj, samples, predictions);    %Generates predictions for a model using samples from variables
        [retPredictions]=computePredictionStatistics(obj, predictions);     %Generates summary statistics for model predictions
        plotData(obj, hPlot, data);                                         %Plots original data for a model (can be overlayed with predicted data)
        plotPredictions(obj, hPlot, data, predictionStatistic);             %Plots predicted data for a model (can be overlayed with original data)
        %determineInitialValues()                                           %Estimates initial values to use for MCMC sampler (via MLE or LS)
    end

    methods (Static)
        function setModel=readStructFromXMLNode(rootNode)     
            stdout=1;
            fprintf(stdout, '\tLoading the model from rootNode\n');
            
            entityname='model';
            rootname=rootNode.getNodeName();
            if ~(strcmp(rootname,entityname)==1)
                error('Root element should be entityname');
            end

            %check version attribute
            tversion=cast(rootNode.getAttribute('version'),'char');
            if (strcmp(mcmcBayes.model.getVersion(),tversion)~=1)
                error('Model version does not match in *.xml');
            end

            %get group, type, subtype attributes
            setModel.group=strcat('mcmcBayes.',cast(rootNode.getAttribute('group'),'char'));
            setModel.type=strcat('mcmcBayes.',cast(rootNode.getAttribute('type'),'char'));
            setModel.subtype=cast(rootNode.getAttribute('subtype'),'char');
        end

        function retObj=constructor(setModel)
            %CONSTRUCTOR
            %   Helper function that uses setModel.type to call the appropriate class constructor for that type
            stdout=1;
            fprintf(stdout, 'Constructing a model object\n');

            modelHandle=mcmcBayes.getClassHandle(setModel.type);   
            retObj=modelHandle(setModel.subtype);
        end        

        function retver=getVersion()%this allows us to read the private constant
            retver=mcmcBayes.model.version;
        end    
    end

    methods
        function obj = model(setGroup, setType, setSubtype)
            %MODEL Construct an instance of this class
            obj.group = setGroup;
            obj.type = setType;
            obj.subtype = setSubtype;
        end

        function [rethPlot]=plotSetup(obj, data, predictions)
            %PLOTSETUP
            %  Creates a plot and returns its handle so that it can be used to plot overlays of original data and model predicted data.
            %
            %  Function can be overridden by subclasses of model.  E.g., for models in the gamma family, data and predictions are 
            %  illustrated via histograms and it is helpful to share the same set of edges for the histogram bins.  The overloaded
            %  for plotSetup function in gammaFamily subclass saves the edges to use for both histograms in an edges field inside the 
            %  hPlot class.
            rethPlot=mcmcInterface.plot();
        end        

        function [retVariables, retData]=transformPre(obj, variables, data)
            %general transformations for variables and data

            %loop through the variables and transform any values.valueConstant
            %loop through the data and transform any values.valueConstant

            %fixed variables-initial values and hyperparameters in retObj.variables.elements(varid).values.valueConstant
            %data elements for posterior updates in retObj.data.elements(datid).values.valueConstant

            %currently not supporting any general mcmcInterface.t_valueTransformPre for variables
            retVariables=variables;

            retData=data;
            for elemid=1:size(retData.elements,1)
                if retData.elements(elemid).values.valueConstant.tfPre==mcmcInterface.t_valueTransformPre.toMeanCentered
                    %data centered at the mean (data-mean(data))
                    warning('only supporting scalar');
                    retData.elements(elemid).values.valueConstant.mean=mean(retData.elements(elemid).values.valueConstant.data);
                    retData.elements(elemid).values.valueConstant.data=retData.elements(elemid).values.valueConstant.data-retData.elements(elemid).values.valueConstant.mean;
                else
                    warning('unsupported mcmcInterface.t_valueTransformPre in transformPre');
                end
                %mcmcInterface.t_valueTransformPre.tolog10              %scaled using log10(data), common logarithm
                %mcmcInterface.t_valueTransformPre.toln                 %scaled using log(data), natural logarithm
                %mcmcInterface.t_valueTransformPre.toStandardized       %data standardized using (data-mean(data))/std(data)
                %mcmcInterface.t_valueTransformPre.tolog10Standardized  %log10(t) data standardized
            end
        end
            
        function [retSamples, retData]=transformPost(obj, samples, data)
            %general transformations for samples and data

            %loop through the samples and transform any values.valueGenerated
            %loop through the data and transform any values.valueConstant

            %generated variables in retObj.samples.elements(varid).values.valueGenerated
            %data elements for posterior updates in retObj.data.elements(datid).values.valueConstant
            
            %currently not supporting any general mcmcInterface.t_valueTransformPre for variables
            retSamples=samples;


            retData=data;
            for elemid=1:size(retData.elements,1)
                if retData.elements(elemid).values.valueConstant.tfPost==mcmcInterface.t_valueTransformPost.fromMeanCentered
                    %data centered at the mean (data-mean(data))
                    warning('only supporting scalar');
                    retData.elements(elemid).values.valueConstant.data=retData.elements(elemid).values.valueConstant.data+retData.elements(elemid).values.valueConstant.mean;
                else
                    warning('unsupported mcmcInterface.t_valueTransformPost in transformPost');
                end
                %mcmcInterface.t_valueTransformPost.fromlog10               %convert back from data scaled using log10(data)
                %mcmcInterface.t_valueTransformPost.fromln                  %convert back from data scaled using log(data)
                %mcmcInterface.t_valueTransformPost.fromStandardized        %convert back from data standardized using (data-mean(data))/std(data)
                %mcmcInterface.t_valueTransformPost.fromlog10Standardized   %convert back from data transformed by taking log10(t) of data that is standardized
                %mcmcInterface.t_valueTransformPost.fromMeanCentered        %convert back from data centered at the mean
            end           
         end    
    end

    methods %getter/setter functions       
        function obj=set.group(obj, setGroup)
            if isempty(which(setGroup))
                error('group %s is not found on MATLAB path',setGroup);
            end
            obj.group=setGroup;
        end 
        
        function obj=set.type(obj, setType)
            if isempty(which(setType))
                error('type %s is not found on MATLAB path',setType);
            end
            obj.type=setType;
        end   
    end        
end

