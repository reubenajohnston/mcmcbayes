classdef basicSequence < mcmcBayes.sequence
    %BASICSEQUENCE
    %   Simple sequence with only a single, vanilla simulation.

    properties
        % No custom properties
    end
    
    methods (Static)
        function retObj=constructor(setSequence)
            %CONSTRUCTOR
            %   Helper function that uses setCfgSequence.type to call the appropriate class constructor for that type
            stdout=1;
            fprintf(stdout, '\tConstructing a basicSequence object\n');

            setModel=mcmcBayes.model.constructor(setSequence.model);
            %construct the simulations array (only one simulation)
            setSimulations(1,1)=mcmcInterface.simulation.constructor(setSequence.simulations(1,1));

            retObj=mcmcBayes.basicSequence(setSequence.uuid, setModel, setSimulations);
        end
    end

    methods
        function obj = basicSequence(setUuid, setModel, setSimulations)
            %basicSequence 
            %   Construct an instance of this class
            if nargin > 0
                superargs{1}='mcmcBayes.basicSequence';
                superargs{2}=setUuid;
                superargs{3}=setModel;
                superargs{4}=setSimulations;
            elseif nargin == 0
                superargs={};
            end
            obj=obj@mcmcBayes.sequence(superargs{:});

        end

        % mcmcBayes.sequence class declares this function as abstract and its children must implement it
        function retObj = runSequence(obj)
            %RUNSEQUENCE
            %   Runs a single, basic simulation for the sequence
            
            obj.simulations(1,1)=obj.simulations(1,1).runSimulation(obj.hModel);    
            retObj=obj;
        end        
    end
end

